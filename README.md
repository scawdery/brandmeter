### The brandmeter frontend
This is a first draft of a possible frontend solution for the brandmeter service. It will be used for users to authenticate all of their services and to visualize data that was inserted via a different microservice to elaticearch.  
The frontend makes use of the next.js server-side rendering framework for react. Next.js also offers a `serverless runtime`. Code, that you can find in the `/pages/api` folder are actually API routes running on the server in a node.js runtime.  
We use `auth0` to store all sensitive user data like access tokens and make use of their libraries to protect backend and frontend routes.

### Configuring Next.js

In the Next.js configuration file (`next.config.js`) you'll see that different environment variables are being assigned.

### Local Development

For local development you'll want to create a `.env` file with the necessary settings.

The required settings can be found on the Auth0 application's settings page:

```
AUTH0_DOMAIN=YOUR_AUTH0_DOMAIN
AUTH0_CLIENT_ID=YOUR_AUTH0_CLIENT_ID
AUTH0_CLIENT_SECRET=YOUR_AUTH0_CLIENT_SECRET

SESSION_COOKIE_SECRET=viloxyf_z2GW6K4CT-KQD_MoLEA2wqv5jWuq4Jd0P7ymgG5GJGMpvMneXZzhK3sL (at least 32 characters, used to encrypt the cookie)

REDIRECT_URI=http://localhost:3000/api/callback
POST_LOGOUT_REDIRECT_URI=http://localhost:3000/
```