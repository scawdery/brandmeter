import client from './elasticConnect'

const subquery = network => ({
  "filter": {
    "term": {
      "application.keyword": network
    }
  },
  "aggs": {
    "impressions": {
      "filter": {
        "term": {
          "aggregated_type.keyword": "impressions"
        }
      },
      "aggs": {
        "sum": {
          "sum": {
            "field": "metric_value"
          }
        }
      }
    },
    "engagements": {
      "filter": {
        "term": {
          "aggregated_type.keyword": "total_engagements"
        }
      },
      "aggs": {
        "sum": {
          "sum": {
            "field": "metric_value"
          }
        }
      }
    }
  }
});

export default async (brand, fromDate, toDate, filters) => {

  // the query for all networks except instagram
  let query = {
    "aggs": {
      "facebook": subquery("facebook"),
      "linkedin": subquery("linkedin"),
    },
    "size": 0,
    "_source": {
      "excludes": []
    },
    "stored_fields": [
      "*"
    ],
    "script_fields": {},
    "docvalue_fields": [{
      "field": "timestamp",
      "format": "date_time"
    }],
    "query": {
      "bool": {
        "must": [],
        "filter": [{
            "bool": {
              "filter": [
                {
                  "bool": {
                    "should": [{
                      "match_phrase": {
                        "brand_id.keyword": brand
                      }
                    }],
                    "minimum_should_match": 1
                  }
                }
              ]
            }
          },
          {
            "range": {
              "timestamp": {
                "format": "strict_date_optional_time",
                "gte": fromDate,
                "lte": toDate
              }
            }
          }
        ],
        "should": [],
        "must_not": []
      }
    }

  }

  let instagram_engagement = {
    "query": {
      "bool": {
        "must": [],
        "filter": [{
            "bool": {
              "filter": [
                {
                  "bool": {
                    "should": [{
                      "match_phrase": {
                        "brand_id.keyword": brand
                      }
                    }],
                    "minimum_should_match": 1
                  }
                }
              ]
            }
          },
          {
            "range": {
              "timestamp": {
                "format": "strict_date_optional_time",
                "gte": fromDate,
                "lte": toDate
              }
            }
          }
        ],
        "should": [],
        "must_not": []
      }
    },
    "size": 0,
    "aggs": {
      "impressions": {
        "sum": {
          "field": "impressions"
        }
      },
      "engagements": {
        "sum": {
          "field": "engagement"
        }
      }
    }
  }
  

 

  // push the page filters
  query.query.bool.filter.push(filters)

  instagram_engagement.query.bool.filter.push(filters)

  try {
  
    let result = await client.msearch({
      body: [
        {index: 'brandmeter-elasticdayout'},
        query,
        {index: 'brandmeter-posts'},
        instagram_engagement

      ]
    })

    let result0 = result.body.responses[0].aggregations
    Object.keys(result0).map(network => {
      if(result0[network].impressions && result0[network].engagements) {
        result0[network].impressions = result0[network].impressions.sum.value
        result0[network].engagements = result0[network].engagements.sum.value
        result0[network].engagementrate = result0[network].engagements / result0[network].impressions
      }
    })


    if(result.body.responses[1].aggregations){
      let result1 = result.body.responses[1].aggregations
      if(result1.impressions && result1.engagements) {
        result0.instagram = {
          impressions: result1.impressions.value,
          engagements: result1.engagements.value,
          engagementrate: result1.engagements.value/result1.impressions.value
        }
      }
    }

    // the total average
    const total = Object.values(result0).reduce((total, cur)=> ({eng:total.eng + cur.engagements, imp:total.imp+cur.impressions}),{eng:0, imp:0})
    return { value: Math.round((total.eng/total.imp * 100 + Number.EPSILON) * 100) / 100 } //rundet mit 2 kommastellen. EPSILON um floatingpoint rundungsfehler bei .005 zu vermeiden



  } catch (error) {
    console.error('Error getting engagement data from elastic', error)
    if(error.name === 'ResponseError') console.log(error.meta.body)
    
  }




}