import client from './elasticConnect'
import cloneDeep from 'lodash/cloneDeep'


export default async (brand, date, filters) => {
  try {
    let body = {
        "aggs":{
          "time":{
            "auto_date_histogram":{
              "field": "timestamp",
              "buckets": 80,
              "minimum_interval": "day",
              "format": "dd.MM.yy",
              "time_zone": "Europe/Berlin"
            },
            "aggs":{
              "metric":{
                "terms": {
                  "field": "aggregated_type.keyword",
                  "size": 10
                },
                "aggs": {
                  "network": {
                    "terms": {
                      "field": "application.keyword",
                      "size": 10
                    },
                    "aggs": {
                      "value": {
                        "sum": {
                          "field": "metric_value"
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "size": 0,
        "docvalue_fields": [{
              "field": "timestamp",
              "format": "date_time"
        }],
        "query": {
          "bool": {
            "must": [],
            "filter": [
              {
                "bool": {
                  "filter": [
                    {
                      "bool": {
                        "should": [
                          {
                            "match_phrase": {
                              "brand_id.keyword": brand
                            }
                          }
                        ],
                        "minimum_should_match": 1
                      }
                    }
                  ]
                }
              },
              {
                "range": {
                  "timestamp": {
                    "format": "strict_date_optional_time",
                    "gte": date.fromDate,
                    "lte": date.toDate
                  }
                }
              }
            ],
            "should": [],
            "must_not": []
          }
        }
    }

    // push the page filters
    body.query.bool.filter.push(filters)

    // create the query body for the compare time
    let compareQuery = cloneDeep(body)
    compareQuery.query.bool.filter[1].range.timestamp = { format: 'strict_date_optional_time', gte: date.compareTimefromDate, lte: date.compareTimetoDate}    

    let results = await client.msearch({
      body: [
        {index: 'brandmeter-elasticdayout'}, body,
        {index: 'brandmeter-elasticdayout'}, compareQuery
      ]
    })

    if(!results.body.responses) throw new Error('Didn\'t get a valid response from Elasticsearch', result)

    results = results.body.responses


    // what we return later
    let response = {}
    

    // setting the aggregation interval. Like 1d or 7d
    response.interval = results[0].aggregations['time'].interval

    // go through the current and the compare time intervall
    results = results.map( result => {

      result = result.aggregations['time']

     
      result.buckets.map( time_intervall => {
        // time_intervall['key_as_string'] is the current day like 01.01.20
       time_intervall.metric.buckets.map( metric => {
          // metric.key is the current metric name like impressions
          // initialize the empty object we push all the data into
          if(!result[metric.key] || !result[metric.key].totals) result[metric.key] = {
              merged_buckets: [],
              totals: {
                networks: {},
                total: 0
              }
            }
      
          // network.key is the current network like facebook
          metric.network.buckets.map(network => {
            if(!result[metric.key].totals.networks[network.key] || !result[metric.key].totals.networks[network.key].total ) result[metric.key].totals.networks[network.key] = { total: 0 }
            result[metric.key].totals.networks[network.key].total += network.value.value
            result[metric.key].totals.total += network.value.value
          })
          let push = {}
          push = metric.network.buckets.reduce((obj, item) => {
              obj[item.key] = item.value.value
              return obj
            }, {})
          push.total = Object.values(push).reduce((a, b) => a + b, 0)
          push.date = time_intervall['key_as_string']
          result[metric.key]['merged_buckets'].push(push)
          
        
          })
    

      })


      delete result.interval
      delete result.buckets

      return result

    })

    
    for (const metric in results[0]){
      
      if(!results[0][metric]|| !results[1][metric]){
        continue
      } else{
        results[0][metric]['allnetworks'] = Object.keys(results[0][metric].totals.networks)
      }
  
      // set the changerate per network
      for (const network in results[0][metric].totals.networks){
        
        if(!results[1][metric]['totals']['networks'][network] || !results[0][metric]['totals']['networks'][network] ) {
          results[0][metric]['totals']['networks'][network].changerate = null
        } else {
          results[0][metric]['totals']['networks'][network].changerate = changerate(results[1][metric]['totals']['networks'][network].total, results[0][metric]['totals']['networks'][network].total)
        }
        
      }

      if(!results[0][metric]['totals'] || !results[1][metric]['totals']){
        results[0][metric]['totals']['changerate'] = null  
      } else {
        results[0][metric]['totals']['changerate'] = changerate(results[1][metric].totals.total ,results[0][metric].totals.total)
      }

    }
    
    // results[0] is the current time period and results[1] the compare
    // time period
    Object.assign(response, results[0])
  

    return response



  } catch (error) {
    console.error('Error getting aggregated metrics', error)
    if (error.name === 'ResponseError') console.log(error.meta.body)
    return null

  }


}


const changerate = (before,after) => {

  return  +parseFloat(((after-before) / before).toFixed(3))

}  