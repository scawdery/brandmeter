import client from './elasticConnect'
import AWS from 'aws-sdk'

const endpoint = new AWS.Endpoint(process.env.S3_URL);
const s3 = new AWS.S3({
  endpoint: endpoint,
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY,
  s3ForcePathStyle: true,
  region: 'nue1'
});

export default async (brand, date, filters) => {
  let response = {
    topPosts: {}
  }

  let TopPostsQuery = {
    "size": 60,
    "query": {
      "bool": {
        "must": [],
        "filter": [{
            "bool": {
              "filter": [{
                "bool": {
                  "should": [{
                    "match_phrase": {
                      "brand_id.keyword": brand
                    }
                  }],
                  "minimum_should_match": 1
                }
              }]
            }
          },
          {
            "range": {
              "timestamp": {
                "format": "strict_date_optional_time",
                "gte": date.fromDate,
                "lte": date.toDate
              }
            }
          }
        ]
      }
    },
    "sort": [{
      "engagement_rate": {
        "order": "desc"
      }
    }]
  }

  // push the page filters
  TopPostsQuery.query.bool.filter.push(filters)

  let params = {
    Bucket: process.env.S3_BUCKET_NAME,
  }

  try {
    let result = await client.search({
      index: 'brandmeter-posts',
      body: TopPostsQuery
    })
    if (result.body.hits.hits.length < 1) return response.topPosts = null
    result = result.body.hits.hits.map(post => {
      post = post._source
      params.Key = post.post_id + '.jpg'
      post.s3url = s3.getSignedUrl('getObject', params)
      return post
    })


    return {
      topPosts: result
    }

  } catch (error) {
    console.error('Error getting posts from elastic', error)
    if (error.name === 'ResponseError') console.log(error.meta.body)

  }


}