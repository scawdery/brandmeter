import { useState, useEffect, createContext } from 'react'
import fetch from 'isomorphic-unfetch'
// import createPersistedState from 'use-persisted-state';
// const useCounterState = createPersistedState('user');
// const useCounterStateLoading = createPersistedState('loading');

// Use a global to save the user, so we don't have to fetch it again after page navigations
let userState;

const User = createContext({ user: null, loading: false });


// helper function to get more user profile data from auth0 management API
export async function fetchIdp( dialogOpen, cookie = '' ) {
  if (userState !== undefined) {
    return userState;
  }

  const res = await fetch(
    '/api/idpdata?'+'dialogOpen='+dialogOpen,
    {
      headers: {
            dialogOpen
          }
    }
  )
  if (!res.ok) {
    delete window.__user
    return {unauthorized: true}
  }
  const json = await res.json()
  if (typeof window !== 'undefined') {
    window.__user = json
  }
  return {data:json}
}


export const UserProvider = ({ value, children }) => {
  const { user } = value;

  // If the user was fetched in SSR add it to userState so we don't fetch it again
  useEffect(() => {
    if (!userState && user) {
      userState = user;
    }
  }, []);

  return <User.Provider value={value}>{children}</User.Provider>;
};

export const useUser = () => useContext(User);



// Get's called by different pages to fetch all user information available. Forwards to /login if not authenticated
export function useFetchUser({ required, dialogOpen = false } = {}) {

  const [user, setUser] = useState(
    userState || null
  );
  const [loading, setLoading] = useState(
    userState === undefined
  )



  useEffect(
    () => {
      if (userState !== undefined) {
        return;
      }
     
      
      let isMounted = true
      const fetchdata = async () => {

         // going to the backend and loading all profile data
         let idpdata = await fetchIdp(dialogOpen)
         if(idpdata.data) {
           
           setUser(idpdata.data)
           userState = idpdata.data
           setLoading(false)
           // set also the global user object
         } else{
           setUser(false)
           setLoading(false)
         }
       
        // Only set the user if the component is still mounted
        if (isMounted) {
        // When the user is not logged in but login is required
          if (required && idpdata.unauthorized) {
              window.location.href = '/api/login'
                return
            }
          
        }

      
      }

      fetchdata()

      return () => {
        isMounted = false
      }


    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  
  return { user, loading }
}
