import fetch from 'node-fetch'
import * as Parallel from 'async-parallel';



export default async function getPages(identities){
  

    // go parallel through all identities and ask the identity providers for all page IDs we have access to.
    await Parallel.each(identities, async item => {
        if(item.provider === 'facebook'){
            let result = await fetch('https://graph.facebook.com/v5.0/me/accounts?access_token='+item.access_token)
            result = await result.json()
         
            let pages = []
            
            // get resulting instagram pages per Facebook page
            await Parallel.each(result.data, async page => {
                let res = await fetch('https://graph.facebook.com/v5.0/'+page.id+'?fields=instagram_business_account{id, username,profile_picture_url}&access_token='+item.access_token)
                res = await res.json()
                if(res && res.instagram_business_account){
                    res.instagram_business_account.name = res.instagram_business_account.username
                    res.instagram_business_account.type = 'instagram'
                    res.instagram_business_account.provider = 'facebook'
                    res.instagram_business_account.user_id = item.user_id
                    res.instagram_business_account.facebook_page_id = page.id
                    pages.push(res.instagram_business_account)
                    
                }

            })
            result = result.data.map( e => {
                e.type = 'facebook'
                e.provider = 'facebook'
                e.user_id = item.user_id
                return e
            })
            pages = pages.concat(result)
            
           
            item.pages = pages
          
        }
        if(item.provider === 'linkedin'){
            // get the pages we have access to
            let result = await fetch('https://api.linkedin.com/v2/organizationalEntityAcls?q=roleAssignee',{
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+item.access_token }
            })
            result = await result.json()
            if (result && result.elements){
                let pages = []
                // for every page we take further information like name etc.
                await Parallel.each(result.elements, async page => {
                    const id = page.organizationalTarget.split(":")
                    let res = await fetch('https://api.linkedin.com/v2/organizations/'+id[3],{
                        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+item.access_token }
                    })
                    res = await res.json()
                    if (res) {
                        res.type = 'linkedin'
                        res.provider = 'linkedin'
                        res.user_id = item.user_id
                        res.name = res.localizedName
                        pages.push(res)
                    }
                })

                item.pages = pages
            }

        }
        if(item.provider === 'google-oauth2'){
            console.log('Getting all Google My Business Accounts with token',item.access_token)
            let allAccounts = await fetch('https://mybusiness.googleapis.com/v4/accounts/', {
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+item.access_token }
            })
            if( !allAccounts.ok ) throw new Error('Error getting All Accounts from Google')
            allAccounts = await allAccounts.json()
            console.log('all accounts', allAccounts)

            for (const account of allAccounts.accounts){
                let result = await fetch('https://mybusiness.googleapis.com/v4/'+account.name+'/locations', {
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+item.access_token }
                })
                result = await result.json()

                let allPages = []
                if(result && result.locations){
                    result = result.locations.map( pages => {
                        pages.id = pages.name
                        pages.name = pages.locationName
                        pages.type = 'googlemybusiness'
                        pages.provider = 'google'
                        pages.user_id = item.user_id
                        return pages
                    })
                    allPages.push(...result)

                }
                console.log('all pages', allPages)


                item.pages = allPages



            }
            
     
            

        }

    })

    return identities[0].pages

}