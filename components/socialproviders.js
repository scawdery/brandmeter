import { FacebookLoginButton, LinkedInLoginButton, GoogleLoginButton } from "react-social-login-buttons";
import { useState, useEffect} from 'react'
import { useRouter } from 'next/router'
import Grid from '@material-ui/core/Grid';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import LinearProgress from '@material-ui/core/LinearProgress';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import BusinessIcon from '@material-ui/icons/Business';
import Button from '@material-ui/core/Button';
import NotInterestedOutlinedIcon from '@material-ui/icons/NotInterestedOutlined';
import DialogActions from '@material-ui/core/DialogActions';
import fetch from 'isomorphic-unfetch'

import { useStateValue } from './globalcontext';
import { useFetchUser } from '../lib/user'


function Simpledialog(props){
    const { dialogNetwork, identyObject, onClose, open, checked, setChecked } = props;

    
    function handleClose() {  
        onClose();
      };
  
    const handleToggle = value => () => {
      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];
  
      if (currentIndex === -1) {
        newChecked.push(value);
      } else {
        newChecked.splice(currentIndex, 1);
      }
  
      setChecked(newChecked);
    }

    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
            <DialogTitle id="simple-dialog-title">Select the accounts you want to add</DialogTitle>
            {!identyObject && <LinearProgress />}
            {identyObject && identyObject[dialogNetwork] && identyObject[dialogNetwork].pages && 
                <List >
                    {identyObject[dialogNetwork].pages.map(value => {
                        const labelId = `checkbox-list-label-${value.name}`;

                        return (
                        <ListItem key={value.name} role={undefined} dense button onClick={handleToggle(value)}>
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={checked.indexOf(value) !== -1}
                                        tabIndex={-1}
                                        disableRipple
                                        inputProps={{ 'aria-labelledby': labelId }}
                                        /> 
                                    </ListItemIcon>
                                    <ListItemIcon>
                                    {value.type ==='instagram'?  <InstagramIcon /> : value.type === 'facebook' ? <FacebookIcon/> : 
                                            value.type === 'linkedin' ? <LinkedInIcon/> : value.type === 'googlemybusiness' ? <BusinessIcon/>:null}
                                    </ListItemIcon>
                                    
                                    <ListItemText id={labelId} primary={`${value.name}`} />

                                </ListItem>
                            
                    );
                })}
                </List> }
                <DialogActions style={{justifyContent: 'center'}}> 
                {identyObject && <Button variant="contained" color="primary" onClick={handleClose}>
                        Confirm
                    </Button>}
                </DialogActions>

        </Dialog>

        )
    
}



function Socialproviders( cookie = '') {

    const [{globaluser}, dispatch] = useStateValue()
    const [identyObject, setidentyObject] = useState(null)
    const router = useRouter()
    
    // the dialog to select your networks should be opened or closed depending on the querystring
    const [dialogOpen, setdialogOpen] = useState(() => {
        if (typeof window === 'undefined') return false
        if( window.location.search && /network=/.test(window.location.search)) return true
        return false 
    })

    const {user, loading} = useFetchUser({dialogOpen})
    
   
    const [dialogNetwork, setdialogNetwork] = useState(() => {
        if(dialogOpen){
            const urlParams = new URLSearchParams(window.location.search)
            const network = urlParams.get('network')
            return network
        }
    })

    // save, what accounts we checked in the dialog
    const [checked, setChecked] = React.useState([]);
    
    // when the user object changes, we set also the "identity object"
    useEffect(() => {
        if(user && user.identities){
            let object = user.identities.reduce(
                (obj, item) => Object.assign(obj, { [item.provider]: item }), {});
            setidentyObject(object)
        }     
    },[user])
    
    const handleClose = value => {
        // when closing the dialog we set the result in the auth0 database via this backend API route
        // we just send over the array of checked identities. 
        fetch('/api/setmetadata', {
            method: 'POST',
            headers: {
              cookie,
              "Content-Type": "application/json"
            },
            body: JSON.stringify(checked)
        })
        
        setdialogOpen(false);
       
        
        // filling the user object with the new pages. This is used to immediately show the user the success of 
        // the action without needing to load data from the backend, or reload the whole page
        checked.map(page => {
            globaluser.app_metadata ? '' : globaluser.app_metadata = {pages: {}}
            globaluser['app_metadata']['pages'][page.type] = {}
            globaluser['app_metadata']['pages'][page.type][page.id] = {"type": page.type}
            dispatch({type: 'setuser', globaluser })
        })
   
        router.push('/')
      };


    
    return(
        <>
        <Simpledialog dialogNetwork={dialogNetwork} identyObject={identyObject} open={dialogOpen} onClose={handleClose} checked={checked} setChecked={setChecked}/>
      
            
            <Grid container spacing={3} alignItems="center">   
                <Grid item xs={5} md={6}>
                    <GoogleLoginButton
                            onClick={() => { return location.href='api/login?provider=google' }}
                        />
                </Grid>
                    <Grid item xs={2}> {!dialogOpen && (loading ? <CircularProgress /> : user.app_metadata && user.app_metadata.pages && user.app_metadata.pages.googlemybusiness  ? <CheckCircleOutlineIcon style={{ color: green[500] }} fontSize="large" /> :  <NotInterestedOutlinedIcon style={{ color: red[500] }} fontSize="large"/> )} </Grid>
                </Grid>
            <Grid container spacing={3} alignItems="center">  
                <Grid item xs={5} md={6}>
                    <FacebookLoginButton
                        onClick={() => { return location.href='api/login?provider=facebook' } }
                        />
                </Grid>
                    <Grid item xs={2}> {!dialogOpen && (!loading && user.app_metadata && user.app_metadata.pages && user.app_metadata.pages.facebook  ? <CheckCircleOutlineIcon style={{ color: green[500] }} fontSize="large" /> :  !loading && (!user.app_metadata || user.app_metadata && !user.app_metadata.facebook) ? <NotInterestedOutlinedIcon style={{ color: red[500] }} fontSize="large"/>  : <CircularProgress />)} </Grid>            
                </Grid>
            <Grid container spacing={3} alignItems="center">
                <Grid item xs={5} md={6}>
                    <LinkedInLoginButton onClick={() => { return location.href='api/login?provider=linkedin' } } />
                </Grid>
                    <Grid item xs={2}> {!dialogOpen && (!loading && user.app_metadata && user.app_metadata.pages && user.app_metadata.pages.linkedin ? <CheckCircleOutlineIcon style={{ color: green[500] }} fontSize="large"/> : !loading && (!user.app_metadata || user.app_metadata && !user.app_metadata.linkedin) ? <NotInterestedOutlinedIcon style={{ color: red[500] }} fontSize="large"/> : <CircularProgress />)} </Grid>            
                </Grid>
        
        </>
    )


}

export default Socialproviders