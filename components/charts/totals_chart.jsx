import { ResponsiveContainer, LineChart, Line, CartesianGrid, Legend,  XAxis, YAxis, Tooltip } from 'recharts';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';


export default ({ data, colorsandicons }) => {
    
    function CustomTooltip({ payload, label, active }) {
        if (active) {
          return (
            <div className="custom-tooltip">
                <Card>
                    <CardContent>
                        <p className="label"> <Typography>{label}</Typography></p>
                        { payload.map(value => 
                            <Typography>
                                {value.name} : {value.value}
                            </Typography>
                            
                        ) }
                        <Typography>
                            <Box fontWeight="fontWeightMedium">
                                Total: {payload[0].payload.total}
                            </Box>
                        </Typography>

                    </CardContent>
                    

                </Card>
              
            
            </div>
          );
        }
      
        return null;
    }
    return (
        <ResponsiveContainer width='100%'  minHeight='200' aspect={2/0.8}>      
            {data && data.allnetworks ? <LineChart data={data.merged_buckets} 
                                            margin={{ top: 20, right: 30, left: -10, bottom: 10 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        {/* <Line yAxisId="left" type="monotone" key="total" connectNulls stackId="1" dataKey="total" stroke="grey" strokeDasharray="3 3" strokeWidth={3} dot={false}/> */}
                        {data.allnetworks && data.allnetworks.map((network, index) => {
                            return (<Line yAxisId="left" key={network} type="monotone" connectNulls stackId="1" dataKey={network} fill={colorsandicons[network].color} stroke={colorsandicons[network].color} strokeWidth={3} dot={false} />)
                            })
                        }                        
                        <Tooltip content={<CustomTooltip />} />
                        <XAxis dataKey="date" minTickGap={10} />
                        <YAxis type="number" domain={['dataMin', '3000']} yAxisId="left" />
                        <Legend iconType="square" />
                    </LineChart> : <CircularProgress />}
        </ResponsiveContainer>
    )


}