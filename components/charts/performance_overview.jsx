import { LineChart, Line } from 'recharts';


export default ({ data }) => {
    return(
       
        <LineChart data={data} width={250} height={50}>
            <Line key="total" dataKey="total" dot={false} strokeWidth={2} />
        </LineChart>

    )




}