import { StateProvider } from '../components/globalcontext';
import CssBaseline from '@material-ui/core/CssBaseline'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Head from 'next/head';
import * as Sentry from '@sentry/node'
import '../css/recharts.scss'

Sentry.init({
  enabled: process.env.NODE_ENV === 'production',
  dsn: process.env.SENTRY_DSN,
})

const black = "#343a40";
const darkBlack = "rgb(36, 40, 44)";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1D326A',
    },
    common: {
      black,
      darkBlack
    },
  },
  typography: {
    h5: {
      fontSize: "1.3rem"
    }
  },
  status: {
    danger: '#E56F35',
  }
});


const MyApp = ({ Component, pageProps, err }) =>  {
    const initialState = {
        globaluser: {}
      };
      
    const reducer = (state, action) => {
        switch (action.type) {
            case 'setuser':
            return {
              ...state,
              globaluser: action.globaluser
            };
            
          default:
            return state;
        }
    }


    const modifiedPageProps = { ...pageProps, err }


    
    return (
        <StateProvider initialState={initialState} reducer={reducer}>
          <ThemeProvider theme={theme}>
            <Head>
              <title>brandmeter frontend</title>
              <link rel="preload" as="image" href="logoanimation_klein.png" />
            </Head>
                <CssBaseline />
                  <Component {...modifiedPageProps} />
          </ThemeProvider>
        </StateProvider>  
    )
  }


export default MyApp