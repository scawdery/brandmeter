// The overview over all the brand reach.
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import dayjs from 'dayjs'
import dynamic from 'next/dynamic'
import Backdrop from '@material-ui/core/Backdrop';
import LazyLoad from 'react-lazyload';


import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import InstagramIcon from '@material-ui/icons/Instagram';

import Drawer from '../components/drawer'
import SummaryTable from '../components/charts/overview_table'

const TopPosts = dynamic(() => import('../components/charts/overview_posts'))
const Demographics = dynamic(() => import('../components/demographics'))


import PerformanceChart from '../components/charts/performance_overview'
import useSWR from 'swr'
import fetch from 'unfetch'

import TotalsChart from '../components/charts/totals_chart'
import { useFetchUser } from '../lib/user'
import Pickersandfilter from '../components/pickerandfilter'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider'

import createPersistedState from 'use-persisted-state';
const useActiveBrand = createPersistedState('activeBrand')
const useSwitchBrandDialog = createPersistedState('switchBrand')


const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})


const useStyles = makeStyles(theme => ({
    root:{
        display: "flex"
    },
    chartPaper:{
        padding: theme.spacing(2)
    },
    cardHeader:{
        title: {
            fontSize: "10rem"
        },
        subheader: {
            fontSize: 12
        }
    },
    fab: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
    card: {
        width: '100%',
        // minHeight: 105,
        maxWidth: 142,
        padding: 1
    },
    content: {
        padding: theme.spacing(2),
        flexGrow: 1
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    // needed to shift content below appbar
    toolbar: theme.mixins.toolbar

}));



function BrandOverview(){
    const classes = useStyles();
 
    const [activeBrand, setActiveBrand] = useActiveBrand({})
    const [switchBrandopen, setswitchBrandopen] = useSwitchBrandDialog(() => activeBrand.brand ? false : true);

    // you need to select a brand if you did not now
    React.useEffect(() => {
        if (!activeBrand.brand) setswitchBrandopen(true)
    }, [activeBrand] )

    const [selectedDate, handleDateChange] = React.useState([dayjs().subtract(1, 'month'), dayjs().subtract(1, 'days')]);


    const { user, loading } = useFetchUser({required: true})
    
    // default are all networks checked
    const [checkedPages, setcheckedPages] = React.useState()
    const [activeTab, setActiveTab] = React.useState(0)
   



    const { data } = useSWR(checkedPages && selectedDate ? '/api/getmetrics?fromDate='+selectedDate[0].toISOString()+'&toDate='+selectedDate[1].toISOString()+'&networks='+encodeURIComponent(JSON.stringify(checkedPages))+'&brand='+activeBrand.id : '', fetcher)
    const { data: postData } = useSWR(checkedPages && selectedDate ? '/api/getposts?fromDate='+selectedDate[0].toISOString()+'&toDate='+selectedDate[1].toISOString()+'&networks='+encodeURIComponent(JSON.stringify(checkedPages))+'&brand='+activeBrand.id : '', fetcher)

    // For security: remove the last value of the follower count array, as it is not clean
    if(data && data.follower_count) data.follower_count.merged_buckets.pop()

    const colorAndIcons = {
        facebook: {
            icon: <FacebookIcon fontSize='large'/>,
            color: '#45AAF2'
        },
        instagram: {
            icon: <InstagramIcon fontSize='large'/>,
            color: '#FF0800'
        },  
        linkedin: {
            icon: <LinkedInIcon fontSize='large'/>,
            color: '#8854D0'
        },
        googlemybusiness: {
            icon: <BusinessIcon fontSize='large'/>,
            color: '#20BF6B'
        }
    }
    
    return (
        <div className={classes.root}> 
            <Drawer  />
                    {/* the fullscreen loading overlay */}
                <Backdrop className={classes.backdrop} open={data && data.impressions ? false : true }>
                    {activeBrand.brand && <img src='/logoanimation_klein.png' />}
                </Backdrop>
            <main className={classes.content} >
                <div className={classes.toolbar} /> 
                <Box >
                    <Container maxWidth="xl">
                        <Pickersandfilter 
                            selectedDate={selectedDate}
                            handleDateChange={handleDateChange}
                            setcheckedPages={setcheckedPages}
                            demographics={true}
                            setActiveTab={setActiveTab}
                        />
                        { activeTab === 0 && 
                            <Grid container spacing={2} >
                                <Grid item style={{width: "100%"}}>
                                    <Card variant='outlined'>
                                        <CardHeader
                                            className={classes.cardHeader}
                                            title='Performance Overview'
                                            subheader='See the most important performance statistics. Actions describe all profile interactions like phone calls, navigations or website clicks'
                                        />
                                        <Divider/>
                                        <CardContent>
                                            <Grid container justify="space-around">
                                                <Grid item sm={2}>
                                                    <Typography variant="subtitle2" align="center">
                                                        Total Followers:
                                                    </Typography>
                                                    { data && data.follower_count ? 
                                                        <>
                                                        <Typography variant="h5" align="center">
                                                        {data.follower_count.merged_buckets.length > 1 ? data.follower_count.merged_buckets.pop().total : 'No data ..' }
                                                        </Typography>
                                                        <PerformanceChart data={data.follower_count.merged_buckets} /> </> :
                                                        <Typography align="center">
                                                            No data ..
                                                        </Typography>
                                                    }

                                                </Grid>
                                                <Grid item sm={2}>
                                                    <Typography variant="subtitle2" align="center">
                                                        Profile Actions:
                                                    </Typography>
                                                    { data && data.total_engagements ?
                                                        <>
                                                        <Typography variant="h5" align="center">
                                                            {data && data.total_engagements && data.total_engagements.totals.total}
                                                        </Typography>
                                                        <PerformanceChart data={data.total_engagements.merged_buckets} />
                                                        </> :
                                                        <Typography align="center">
                                                            No data ..
                                                        </Typography>
                                                    }
                                                </Grid>
                                                <Grid item sm={2}>
                                                    <Typography variant="subtitle2" align="center">
                                                        Engagement Rate:
                                                    </Typography>
                                                    <Typography variant="h5" align="center">
                                                        {data && data.engagementRate && data.engagementRate.value}%
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>
                                
                                <Grid item style={{width: "100%"}}>
                                    <Card variant='outlined' className={classes.fullsizeCards}>
                                        <CardHeader
                                            title='Total Impressions'
                                            subheader='Total Impressions over all accounts in the selected time period'
                                        />
                                        <Divider/>
                                        <CardContent>
                                            <Grid container alignItems="center">
                                                <Grid item sm={11} >
                                                    {data && data.impressions && <TotalsChart data={data.impressions} colorsandicons={colorAndIcons}/>}
                                                </Grid>
                                    
                                                <Grid item sm={11} >
                                                    {data && data.impressions && <SummaryTable data={data.impressions.totals} entity='Impressions'/>}
                                                </Grid>
                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>

                                <Grid item style={{width: "100%"}}>
                                    <Card variant='outlined' className={classes.fullsizeCards}>
                                        <CardHeader
                                            title='Total Engagements'
                                            subheader='See how people are engaging with your posts and stories during the reporting period.'
                                        />
                                        <Divider/>
                                        <CardContent>
                                            <Grid container alignItems="center">
                                                <Grid item sm={11} >
                                                    {data && data.total_engagements && <TotalsChart data={data.total_engagements} colorsandicons={colorAndIcons}/> }
                                                </Grid>
                                   
                                                <Grid item sm={11} >
                                                    {data && data.total_engagements && <SummaryTable data={data.total_engagements.totals} entity='Engagements'/>}
                                                </Grid>
                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>
                                <Grid item style={{width: "100%"}}>
                                    <LazyLoad>
                                        <TopPosts data={postData} icons={colorAndIcons}/>
                                    </LazyLoad>
                                    
                                </Grid>

                                </Grid>
                        }
                        {
                            activeTab === 1 &&
                            <Demographics />
                        }
                                
                            </Container>
                        
                        </Box>
                    </main>
                </div>

       
    )



}
export default BrandOverview;
