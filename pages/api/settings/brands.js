import auth0 from '../../../lib/auth0'
import client from '../../../lib/elasticsearch/elasticConnect'
import assert from 'assert'





export default auth0.requireAuthentication(async (req, res) => {
    const { method, body } = req
    const user = (await auth0.getSession(req)).user
    
    if (method === 'GET'){
        let result = await client.search({
            index: 'brandmeter-user-data',
            body: {
                query: {
                    bool: {
                        must: [
                            {
                                match: {
                                    brand: true
                                }
                            },
                            {
                                bool: {
                                    should: [
                                        {
                                            match: {
                                                "auth0id.keyword": user.sub
                                            }
                                        },
                                        {
                                            match: {
                                                "sharedWith.keyword": user.sub
                                            }
                                        }

                                    ]
                                }
                            }

                        ] 
                    }
                },
                sort: [
                    {
                    "name.keyword": {
                        "order": "desc"
                        }
                    }
                ]  
            }
            
        })
        assert.strictEqual(result.statusCode, 200)
        let value = result.body.hits.hits.map( result => {
            let x = result._source
            x.id = result._id
            return x
        })

        return res.status(200).send(value)



    }
    if (method === 'POST'){
        if(!body.name) throw new Error('Did not receive a brand name to add')
        
        let result = await client.index({
            index: 'brandmeter-user-data',
            refresh: true,
            body: {
                auth0id: user.sub,
                brand: true,
                name: body.name,
                description: body.description
            }
        })
        assert.strictEqual(result.statusCode, 201)
        return res.status(200).send(result.body)

        

    }




})