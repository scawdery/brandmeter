import getPosts from '../../lib/elasticsearch/getPosts'
import auth0 from '../../lib/auth0'
import dayjs from 'dayjs'



export default auth0.requireAuthentication(async function getposts(req, res) {

    if (!req.query) throw new Error('No mandatory parameters found')
    const {
        query: {
            fromDate,
            toDate,
            networks,
            brand
        }
    } = req;


    if (!(new Date(fromDate) <= new Date())) throw new Error("invalid input fromDate"); //if provided date is not a date or in the future
    if (!(new Date(toDate) <= new Date())) throw new Error("invalid input toDate");
    if (!(new Date(fromDate) <= new Date(toDate))) throw new Error("fromDate should be lower as toDate");

    let user = await auth0.getSession(req)
    user = user.user


    let filters = []
    let response = {}


    if (networks && networks !== 'undefined') {
        // filter for only enabled networks
        let enabledPages = JSON.parse(networks)


        for (const page of enabledPages) {
            if (page === undefined) continue
            filters.push({
                "match_phrase": {
                    "page_id.keyword": page
                }
            })
        }
        filters = {
            "bool": {
                "should": filters,
                "minimum_should_match": 1
            }
        }


    } else {
        // no enabled pages. We send back a valid response, thats all
        return res.status(200).send(response)

    }



    // reset the time in date to 00:00
    let date = {
        toDate,
        fromDate: dayjs(fromDate).startOf('day').toISOString()
    }
    let posts = await getPosts(brand, date, filters)
    response.posts = posts







    return res.status(200).send(response)












})