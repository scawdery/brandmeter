import auth0 from '../../lib/auth0'


export default async function callback(req, res) {
  try {
   
    let redirect = '/'
  
    if(req.query.state){
      try {
        const state = JSON.parse(req.query.state)
        if(state.network){
          console.log('state.network', state.network)
          switch (state.network){
            case 'facebook':
              redirect = '/profile?network=facebook'
              delete state.network
              req.query.state = JSON.stringify(state)
              break
            case 'google':
              redirect = '/profile?network=google'
              delete state.network
              req.query.state = JSON.stringify(state)
              break
            case 'linkedin':
              redirect = '/profile?network=linkedin'
              delete state.network
              req.query.state = JSON.stringify(state)
              break
          }
        } 
        
      } catch(e){
        console.log(e)
      }

    }
      
    await auth0.handleCallback(req, res, { redirectTo: redirect })

  
    
  } catch (error) {
    console.error(error)
    res.status(error.status || 500).end(error.message)
  }
}
