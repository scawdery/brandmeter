import auth0 from '../../lib/auth0'


export default async function login(req, res) {
  let state = {}
    if (typeof window === 'undefined') {
      try {
        const { user } = await auth0.getSession(req);
        if (user) { 
          
          // here be prepare the state that we let auth0 set. When coming back from the login with a different user we are able to 
          // merge the different user ids using this state
          state['mergeuser'] = user.sub
         
          
        }
        
      } catch (error) {
        console.log('error getting user, most likely not logged in yet. No Problem.', error)
        
      }
    
     
    }

  try {
  
    switch ( req.query.provider ) {
      case 'google':
        state['network'] = 'google'
        await auth0.handleLogin(req, res, {
          refetch: true,
          authParams: {
            scope: 'openid profile',
            connection: 'google-oauth2',
            connection_scope: 'https://www.googleapis.com/auth/business.manage',
            access_type: 'offline',
            prompt: 'consent',
            state: JSON.stringify(state)
          }
        })
        
        break;
      case 'facebook':
        state['network'] = 'facebook'
        await auth0.handleLogin(req, res, {
          authParams: {
            scope: 'openid profile',
            connection: 'facebook',
            connection_scope: 'read_insights,manage_pages,pages_show_list,instagram_basic,instagram_manage_comments,instagram_manage_insights,leads_retrieval,public_profile,business_management',
            access_type: 'offline',
            network: 'facebook',
            state: JSON.stringify(state)
          }
        })

        break
      
      case 'linkedin':
        state['network'] = 'linkedin'
        await auth0.handleLogin(req, res, {
          authParams: {
            scope: 'openid profile',
            connection: 'linkedin',
            connection_scope: 'rw_organization_admin,r_organization_social',
            access_type: 'offline',
            state: JSON.stringify(state)
          }
        })

        break

    
      default:
        await auth0.handleLogin(req, res)
        break;
    }
    
  } catch (error) {
    console.error(error)
    res.status(error.status || 500).end(error.message)
  }
}
