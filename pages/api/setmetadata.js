// This API route is used to persistently save metadata of a user to auth0
// in the body we recieve an identity object that we transform and write to auth0 app_metadata

import auth0 from '../../lib/auth0'
import management from '../../lib/auth0management'
import fetch from 'node-fetch'


export default auth0.requireAuthentication(async function setmetadata(req, res) {

  // get the main user-id of the current user
  const answer = await auth0.getSession(req)
  const search = 'identities.user_id:' + '"' + answer.user.sub.split('|')[1] + '"'
  let main_user = await management.getUsers({
    q: search,
    search_engine: 'v3'
  })
  main_user = main_user[0]

  console.log('Setting Metadata for', main_user.user_id)
  try {
    if (req.body && req.body.length > 0) {
      // if the metadata currently don't include the 'page' key we create it
      main_user.app_metadata ? '' : main_user.app_metadata = {}
      main_user.app_metadata.pages ? '' : main_user.app_metadata.pages = {}
      req.body.map(identity => {
        // we always reset all pages to reflect the user choice
        main_user.app_metadata.pages[identity.type] = {}
        main_user.app_metadata.pages[identity.type][identity.id] = {
          type: identity.type,
          user_id: identity.user_id,
          name: identity.name,
          page_id: identity.id
        }
      })
    
      await management.updateAppMetadata({
        id: main_user.user_id
      }, { ...main_user.app_metadata })

      // calling now the backend to immediately pull data
      for (const page of req.body){
        
        const params = new URLSearchParams({
          page: page.id,
          type: page.type,
          user_id: main_user.user_id
        })
        let result = await fetch(process.env.ETL_URL + '?' + params)
        console.log('Status Code of the ETL trigger:', result.status)
        result = await result.json()
        console.log('result', result)

      }

    }


  } catch (error) {
    console.error('Error updating metadata', error)

  }


  return res.status(200).json({
    status: 'success'
  })


});