import React from 'react'

import { useFetchUser } from '../lib/user'
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider';
import Drawer from '../components/drawer'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import SvgIcon from '@material-ui/core/SvgIcon'
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import Tooltip from '@material-ui/core/Tooltip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';


import dynamic from 'next/dynamic'

const PageLoginDialog = dynamic(() => import('../lib/loginPageDialog'))
const ShareDialog = dynamic(() => import('../lib/brandShareandEditDialog').then((mod) => mod.ShareDialog))
const EditDialog = dynamic(() => import('../lib/brandShareandEditDialog').then((mod) => mod.EditDialog))


import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import { useRouter } from 'next/router'
import useSwr from 'swr'
import fetch from 'unfetch'
import Google from '../public/google.svg'

import createPersistedState from 'use-persisted-state';
const useActiveBrand = createPersistedState('activeBrand')
const useBrandLogin = createPersistedState('brandLogin')

const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})


const useStyles = makeStyles(theme => ({
  root:{
      display: "flex",
      padding: theme.spacing(2)
  },
  chartPaper:{
      padding: theme.spacing(2)
  },
  cardHeader:{
      title: {
          fontSize: "10rem"
      },
      subheader: {
          fontSize: 12
      }
  },
  textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '25ch',
  },
  socialbutton:{
    maxWidth: "200px"
  },
  // needed to shift content below appbar
  toolbar: theme.mixins.toolbar

}));


function Profile() {
  const { user, loading } = useFetchUser({ required: true })
  const router = useRouter()
  const [deleteConfirmOpen, setDeleteConfirmOpen] = React.useState(false);
  const [deleteStatus, setDeleteStatus] = React.useState(null)
  const [activeBrand, setActiveBrand] = useActiveBrand({})
  const [shareDialogOpen, setShareDialogOpen] = React.useState(false)
  const [editDialogOpen, setEditDialogOpen] = React.useState(false)
  
  
  // this is all for the dialog that appears when connecting a new network
  // save, what accounts we checked in the dialog
  const [checked, setChecked] = React.useState([]);
  const [brandLogin, setBrandLogin] = useBrandLogin({})
  const [waiting, setWaiting] = React.useState(null)
  const [dialogOpen, setdialogOpen] = React.useState(() => {
    if (typeof window === 'undefined') return false
    if( window.location.search && /network=/.test(window.location.search) && brandLogin) return true
    return false 
  })  
  


  const { data, isValidating, mutate } = useSwr( '/api/settings/brands', fetcher, {refreshInterval: 3000})

  const handleClickOpen = (brand) => {
    setActiveBrand(brand)
    setDeleteConfirmOpen(true);
  };

  const handleClose = async (event) => {
    // when deleting a brand
    if(event.proceed){
      setDeleteStatus('waiting')
      let result = await fetch('/api/settings/brands/'+event.brand,{
        method: 'DELETE'
      })
      if (result.ok ){
        let update = data.filter(x => x.id !== event.brand)
        await new Promise(resolve => {
          setTimeout(resolve, 3000);
        })
        await mutate( update )
      }

    }
    setDeleteConfirmOpen(false);
    setActiveBrand({})
  };


  const handleCloseLoginDialog = async (value) => {
    setWaiting(true)
    let body = {
      activePages:{
      }
    }
    checked.map( page => {
      if (typeof body.activePages[page.provider] === "undefined") body.activePages[page.provider] = []
      body.activePages[page.provider].push(page)
    })

    let result = await fetch('/api/settings/brands/'+brandLogin.id,{
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
    if (result.ok){
      await mutate({ ...data, ...body})
      setBrandLogin({})
      setdialogOpen(false);
      router.push('/profile')
    } 


    
   
  };


  const classes = useStyles();


  // for better UX - if we have no brand, delete active brand. If we have just one brand, active it
  React.useEffect(() => {
    if (data && data.length <1 && activeBrand){
      setActiveBrand({})
    }
    if (data && data.length === 1){
      setActiveBrand(data[0])
    }
  },[data])


  const LoginRedirect = (network,brand) => {
    setBrandLogin({...brand, network})
    switch (network){
      case "google":
        router.push('/api/login?provider=google')
        break
      case "facebook":
        router.push('/api/login?provider=facebook')  
        break
      case "linkedin":
        router.push('/api/login?provider=linkedin')  
        break
    }
  }


  return (
    <>
      <Drawer />
      <div className={classes.toolbar} /> 
      <div className={classes.root}>
        
        <Container maxWidth="md">
        
          {loading ? <>Loading...</> : 
            <Grid container spacing={2}>
                {data && data.length > 0 ? data.map((brand) =>  
                  <Grid item style={{width: "100%"}}>
                    <Card variant='outlined' >
                        <CardHeader
                            className={classes.cardHeader}
                            title={brand.name}
                            subheader={brand.description || 'No description provided'}
                        />
                        <Divider/>
                        <CardContent>
                          <Grid container spacing="2" justify="space-between">
                            <Grid item>
                              <Grid container alignItems="center" spacing="3">
                                <Grid item>
                                  <Tooltip title="Google MyBusiness and Analytics">
                                    <SvgIcon fontSize="large" component={Google}> </SvgIcon> 
                                  </Tooltip>
                                  
                                </Grid>
                                <Grid item>
                                  <Button variant="outlined" onClick={() => LoginRedirect('google', brand) }>(Re-)Connnect </Button>
                                </Grid>
                              </Grid>
                              <Typography>Connected Pages: </Typography>
                              <List>
                                {
                                  brand.activePages &&  brand.activePages.google ?
                                    brand.activePages.google.map(page => (
                                      <ListItem key={page.name} >
                                        <ListItemIcon>
                                          {page.type === 'googlemybusiness' && <BusinessIcon/> }
                                        </ListItemIcon>
                                        <ListItemText id={page.id} primary={page.name} />
                                      </ListItem> 
                                    ))
                                  : <Typography>No pages connected yet</Typography>
                                }
                              </List>



                            </Grid>
                            <Grid item>
                              <Grid container alignItems="center" spacing="3">
                                <Grid item>
                                  <Tooltip title="Facebook and Instagram">
                                    <FacebookIcon fontSize="large"/>
                                  </Tooltip>
                                  
                                </Grid>
                                <Grid item>
                                  <Button variant="outlined" onClick={() => LoginRedirect('facebook', brand) }>(Re-)Connnect </Button>
                                </Grid>
                              </Grid>
                              <Typography>Connected Pages: </Typography>
                              <List>
                                {
                                  brand.activePages &&  brand.activePages.facebook ?
                                    brand.activePages.facebook.map(page => (
                                      <ListItem key={page.name} >
                                        {page.type === 'facebook' ? <FacebookIcon/> : page.type === 'instagram' && <InstagramIcon /> }
                                        <ListItemText id={page.id} primary={page.name} />
                                      </ListItem> 
                                    ))
                                  : <Typography>No pages connected yet</Typography>
                                }
                              </List>

                            </Grid>
                            <Grid item>
                              <Grid container alignItems="center" spacing="3">
                                  <Grid item>
                                    <LinkedInIcon fontSize="large"/>
                                </Grid>
                                <Grid item>
                                  <Button variant="outlined" onClick={() => LoginRedirect('linkedin', brand) }>(Re-)Connnect </Button>
                                </Grid>
                              </Grid>
                              <Typography>Connected Pages: </Typography>
                              {
                                  brand.activePages &&  brand.activePages.linkedin ?
                                    brand.activePages.linkedin.map(page => (
                                      <ListItem key={page.name} >
                                          <LinkedInIcon/>
                                        <ListItemText id={page.id} primary={page.name} />
                                      </ListItem> 
                                    ))
                                  : <Typography>No pages connected yet</Typography>
                                }


                              
                            </Grid>
                          </Grid>

                 

                        </CardContent>
                        <CardActions>
                          <Button onClick={() => {setActiveBrand(brand); setEditDialogOpen(true)}}>Edit</Button>
                          <Button onClick={() => {setActiveBrand(brand); setShareDialogOpen(true)}}>Share</Button>
                          <Button onClick={() => handleClickOpen(brand)} color="secondary">Delete</Button>
                        
                        </CardActions>
                               
                    </Card>
                  </Grid>)
                    : 
                      <Grid item md={8}>
                        <Card>
                          <CardHeader title={isValidating ? "Please wait, loading your brands ..." : "You don't have any brands yet :("}/>
                          <CardActions>
                            {!isValidating && <Button onClick={() => {router.push('/addbrand')} } > Click here to add your first brand </Button>}
                          </CardActions>
                        </Card>

                      </Grid>
                  
                  }
              
            </Grid>
          }
           <Dialog
             open={deleteConfirmOpen}
             onClose={handleClose}
             aria-labelledby="alert-dialog-title"
             aria-describedby="alert-dialog-description"
            >
             <DialogTitle id="alert-dialog-title" color="warning" >{"Are you sure you want to delete the brand "+activeBrand.name+"?"}</DialogTitle>
             <DialogContent>
               <DialogContentText id="alert-dialog-description">
                 When deleting the brand, we will also delete all of the data associated to it. 
               </DialogContentText>
             </DialogContent>
             <DialogActions>
               <Button onClick={handleClose} color="primary" name="cancel">
                 Cancel
               </Button>
               { deleteStatus === 'waiting' ? <CircularProgress si/> : <Button style={{color: red[500]}} onClick={() => handleClose({proceed:true, brand: activeBrand.id })} color="primary" autoFocus name="proceed">
                 Proceed
               </Button> }
             </DialogActions>
          </Dialog>
           
          <PageLoginDialog waiting={waiting} active={dialogOpen} brand={brandLogin} onClose={handleCloseLoginDialog} checked={checked} setChecked={setChecked}/>
          <ShareDialog active={shareDialogOpen} setActive={setShareDialogOpen} activeBrand={activeBrand} />
          <EditDialog active={editDialogOpen} setActive={setEditDialogOpen} activeBrand={activeBrand} mutate={mutate} />
        
        </Container>
      </div>
    </>
 
  )
}

export default Profile
